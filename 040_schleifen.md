# Module und Schleifen

## Module
Es ist sehr aufwändig, wenn man viele gleichartige Objekte
konstruieren will und später noch Änderungen nötig werden.
aus diesem Grund gibt es *Module*. Ein Modul ist ein fertiger
Baustein, den man mehrfach verwenden kann.

    module baum() {
        color("brown") cylinder(3, 0.2, 0.2);
        translate([0,0,3]){
        color("green") sphere(2, $fn=80);
        }
    }
    
    baum(); /* Hier wird der Baum gezeichnet */
    
![Baum](bilder/baum.png)    
    
Nun kannst du auch eine ganze Reihe Bäume zeichnen:

    translate([0,0,0]) baum();
    translate([3,0,0]) baum();
    translate([6,0,0]) baum();
    translate([9,0,0]) baum();
    translate([12,0,0]) baum();
    
    
Aber selbst das ist noch ziemlich aufwändig ... Zeit für Wiederholungen.

## Schleifen

    for (i = [0:3:12]) {
        translate([i,0,0]) baum();
        }
        
![Allee](bilder/reihe.png)            
        
In der Schleife wird der Anfangswert (0), der Endwert (12) und die Schrittweite (3)
für die Variable *i* festgelegt. Schleifen könenn auch geschachtelt werden,
müssen dann aber verschiedenee Variablen verwenden:


    for (k = [0:3:12]) {
        for (i = [0:3:12]) {
            translate([i,k,0]) baum();
        }
    }

![Wald](bilder/wald.png)    


**Auftrag:**

Versuche dich an folgendem Haus. Für das Dach brauchst du noch ein `scale=0.1`
beim `linear_extrude`! Vielleicht wäre ein Zaun (Schleife!) noch eine gute
Idee? Was ist mit einem Weg und einem Sandkasten? Eine Schaukel und ein Baum?


![Haus](bilder/haus2.png)    
