# Verändern von Form, Lage und Ort

## Ort

Die Verschiebung *translate* haben wir bereits besprochen:

    translate ([0,5,0]) {
        cube(2);
        }
        
## Rotieren

Ganz wichtig ist gleich am Anfang, dass hier die reihenfolge seh
wichtig ist. Die Klammern werden imme von innen nach außen
abgearbeitet. Betrachte folgendes Beispiel:

    cylinder(2 ,0.1,0.1, $fn=50);

    rotate([0,45,0]){
        color("green") cylinder(2,0.1,0.1, $fn=50);
    }


    translate([3,0,0]){
        rotate([0,45,0]){
            color("red") cylinder(2,0.1,0.1, $fn=50);
        }
    }


    rotate([0,45,0]){
        translate([3,0,0]){
            color("blue") cylinder(2,0.1,0.1, $fn=50);
        }
    }

Der grüne Zylinder wurde um 45 Grad um die y-Achse gedreht.
Beim roten und blauen Zylinder wurde die Reihenfolge von Verschiebung 
und Rotation vertauscht. Rotieren erfordert gutes räumliches Vorstellungsvermögen!


![Rotieren und verschieben](bilder/lage.png)
