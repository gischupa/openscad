# Grundformen 2D

Eine Methode zum Erstellen von 3D-Figuren ist, dass man eine Fläche in
die Höhe zieht. Aus einem Kreis kann man z.B. einen Zylinder machen.

Aus einem Dreieck oder Viereck könnte man eine Pyramide erzeugen.

    linear_extrude(height = 10, $fn=80) circle(r = 1);
    
Dieser Befehl hat einige Einstellmöglichkeiten, die eher für 
fortgeschrittene Benutzer geeignet sind.

![Linear extrude](bilder/linear.png)
