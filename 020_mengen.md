# Mengenoperationen

Dass du Körper verschieben kannst, hast du bereits gelernt:

    translate([0,1,0]){
        koerper1;
        koerper2;
        koerper3;   }
        
Es ist aber oft praktischer - oder sogar nötig,
aus den Einzelkörpern einen einzigen Körper zu machen.
Dafür gibt es 3 Befehle:

## Union

Eine *union* ist die Verbindung von Einzelköpern
zu einem neuen Körper:

    union() {
        koerper1;
        koerper2;
        }
        
Auf den ersten Blick ergibt es nicht viel Sinn, weil man die
Körper ja auch durch { ... } bereits bündeln kann - aber manchmal 
ist es nötig. Die anderen Befehle sind klarer!

## Intersection

Eine *intersection* ist der Bereich, den zwei Körper
gemeinsam haben:

    intersection (){
        cube(2);
        cylinder(1,2,2, $fn=100);
    }
    
![Körper schneiden](bilder/inter.png)
    
## Difference

Das entspricht mathematisch einem *Minus*. Mit diesem Befehl
kann man z.B. Löcher bohren:

    difference (){
        cube([3,  3, 0.3], center=true);
        cylinder(5,0.2,0.2, center=true, $fn=100);
    }
    
![Körper subtrahieren](bilder/differ.png)
