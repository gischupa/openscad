# Grundformen

Mit OpenSCAD können zweidimensionale Flächen,
dreidimensionale Körper oder Schriften erstellt werden.
Man verwendet dabei ein Koordinatensystem mit einer x-, y- 
und einer z-Achse.

Die x- und y-Achse bilden die Ebene, die z-Achse zeigt nach oben.

Punkte werden immer in eckiven Klammern mit drei Koordinaten angegeben, auch wenn sie 
in der ebene liegen:

    [2, 4, 6]
    
